package de.awacademy.Blog;

import de.awacademy.Blog.beitrag.BeitragController;
import de.awacademy.Blog.beitrag.BeitragRepository;
import de.awacademy.Blog.kommentar.KommentarRepository;
import de.awacademy.Blog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;


@Controller
public class HomeController {

    private BeitragRepository beitragRepository;
    private KommentarRepository kommentarRepository;


    @Autowired
    public HomeController(BeitragRepository beitragRepository, KommentarRepository kommentarRepository) {

        this.beitragRepository = beitragRepository;
        this.kommentarRepository = kommentarRepository;
    }


    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, Model model) {

        model.addAttribute("beitragList", beitragRepository.findAllByOrderByIdDesc());
        model.addAttribute("kommentare", kommentarRepository.findAllByOrderByIdAsc());

        return "home";
    }
}
