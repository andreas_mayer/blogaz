package de.awacademy.Blog.user;


import de.awacademy.Blog.kommentar.Kommentar;
import org.apache.logging.log4j.message.Message;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    // Eigenschaften

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String username;
    private String password;
    private boolean admin;

    @OneToMany(mappedBy = "user")
    private List<Kommentar> kommentare;


    // Konstruktor

    public User() {

    }


    public User(String username, String password) {

        this.username = username;
        this.password = password;
        this.admin = false;
    }


    // Methoden

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }
}
