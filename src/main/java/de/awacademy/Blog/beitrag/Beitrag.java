package de.awacademy.Blog.beitrag;

import de.awacademy.Blog.kommentar.Kommentar;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
public class Beitrag {

    // Eigenschaften

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String ueberschrift;

    @NotBlank
    private String text;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentare;


    // Konstruktor

    public Beitrag() {

    }

    public Beitrag(String ueberschrift, String text) {

        this.ueberschrift = ueberschrift;
        this.text = text;
    }

    public Beitrag(Long id) {

        this.id = id;
    }


    // Methoden


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }
}
