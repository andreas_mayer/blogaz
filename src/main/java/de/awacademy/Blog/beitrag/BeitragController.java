package de.awacademy.Blog.beitrag;


import de.awacademy.Blog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Controller
public class BeitragController {

    private BeitragRepository beitragRepository;
    private List<Beitrag> beitragList = new LinkedList<>();


    @Autowired
    public BeitragController(BeitragRepository beitragRepository) {
        this.beitragRepository = beitragRepository;
    }


    @GetMapping("/beitrag")
    public String beitrag(@ModelAttribute("sessionUser") User sessionUser, Model model) {

        if (sessionUser != null) {

            if (sessionUser.isAdmin()) {

                model.addAttribute("beitrag", new BeitragDTO("", ""));
                return "beitrag";
            }
        }
        return "redirect:/";

    }


    @PostMapping("/beitrag")
    public String beitrag(@ModelAttribute("sessionUser") User sessionUser,
                          @Valid @ModelAttribute("beitrag") BeitragDTO beitragDTO,
                          BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {

            return "beitrag";
        }

        if (sessionUser != null) {

            if (sessionUser.isAdmin()) {

                Beitrag beitrag = new Beitrag(beitragDTO.getUeberschrift(), beitragDTO.getText());
                beitragList.add(beitrag);
                beitragRepository.save(beitrag);

                return "redirect:/";
            }
        }
        return "redirect:/";
    }
}
