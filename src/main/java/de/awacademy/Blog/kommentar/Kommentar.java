package de.awacademy.Blog.kommentar;

import de.awacademy.Blog.beitrag.Beitrag;
import de.awacademy.Blog.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.Instant;


@Entity
public class Kommentar {

    // Eigenschaften

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min = 1)
    private String text;


    @ManyToOne
    private Beitrag beitrag;

    @ManyToOne
    private User user;


    // Konstruktor

    public Kommentar() {

    }

    public Kommentar(String text){
        this.text = text;
    }


    public Kommentar(User user, String text) {

        this.user = user;
        this.text = text;
    }


    // Methoden

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
