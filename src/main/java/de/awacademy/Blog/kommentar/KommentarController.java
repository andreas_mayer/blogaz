package de.awacademy.Blog.kommentar;


import de.awacademy.Blog.beitrag.Beitrag;
import de.awacademy.Blog.beitrag.BeitragRepository;
import de.awacademy.Blog.user.User;
import de.awacademy.Blog.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;


@Controller
public class KommentarController {

    private KommentarRepository kommentarRepository;
    private BeitragRepository beitragRepository;
    private UserRepository userRepository;


    @Autowired
    public KommentarController(KommentarRepository kommentarRepository, BeitragRepository beitragRepository,
                               UserRepository userRepository) {
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
        this.userRepository = userRepository;

    }


    @GetMapping("/{userId}/beitragId/{beitragId}/kommentar")
    public String kommentar(Model model, @PathVariable Long beitragId, @PathVariable Long userId) {
        model.addAttribute("kommentar", new Kommentar(""));
        model.addAttribute("beitragId", beitragId);
        model.addAttribute("userId", userId);

        return "kommentar";
    }


    @PostMapping("/{userId}/beitragId/{beitragId}/kommentar")
    public String kommentar(@Valid @ModelAttribute("kommentar") Kommentar kommentar, BindingResult bindingResult,
                            Instant postedAt,
                            @PathVariable Long beitragId,
                            @PathVariable Long userId) {
                                if (bindingResult.hasErrors()) {
                                     return "kommentar";
                                }


        Optional<Beitrag> beitrag = beitragRepository.findById(beitragId);
        Optional<User> sessionUser = userRepository.findById(userId);

        kommentar.setBeitrag(beitrag.get());
        kommentar.setUser(sessionUser.get());
        kommentarRepository.save(kommentar);

        return "redirect:/";
    }


    @PostMapping("/userId/{kommentarId}/kommentarLoeschen")
    public String kommentarLoeschen(@PathVariable Long kommentarId, @RequestParam("userId") Long userId) {

        kommentarRepository.deleteById(kommentarId);

        return "redirect:/";
    }
}
