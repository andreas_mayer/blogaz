package de.awacademy.Blog.beitrag;

import javax.validation.constraints.NotBlank;

public class BeitragDTO {


    // Eigenschaften

    @NotBlank
    private String ueberschrift;

    @NotBlank
    private String text;


    // Konstruktor

    public BeitragDTO(@NotBlank String ueberschrift, @NotBlank String text) {

        this.text = ueberschrift;
        this.text = text;
    }


    // Methoden


    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
