package de.awacademy.Blog.beitrag;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BeitragRepository extends CrudRepository<Beitrag, Long> {

    List<Beitrag> findAllByOrderByIdDesc();

}
