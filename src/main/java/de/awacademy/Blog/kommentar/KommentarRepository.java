package de.awacademy.Blog.kommentar;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface KommentarRepository extends CrudRepository<Kommentar, Long> {

    List<Kommentar> findAllByOrderByIdAsc();

    Optional<Kommentar> findAllByIdAndUserId(Long kommentarId, Long userId);
}

