package de.awacademy.Blog.user;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO {

    // Eigenschaften

    @Id
    @GeneratedValue
    private long userId;

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9_]+$", message = "Darf nur Buchstaben, Zahlen und den Unterstrich enthalten.")
    private String username;

    @Size(min = 5)
    private String password1;
    private String password2;

    private boolean isAdmin;


    //Konstruktoren

    public UserDTO(String username, String password1, String password2, boolean isAdmin) {

        this.username = username;
        this.password1 = password1;
        this.password2 = password2;
        this.isAdmin = false;
    }

    public UserDTO() {

    }


    //Methoden


    public long getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword1() {
        return password1;
    }

    public String getPassword2() {
        return password2;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
